<%-- 
    Document   : clienteAtendimentoLink
    Created on : 28/09/2019, 16:22:52
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="robots" content="index, follow"/>

        <title>Embuste</title>

        <!-- Simbols -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Style -->
        <link rel="stylesheet" href="../css/reset.css">
        <link rel="stylesheet" href="../css/styles.css">

    </head>

    <body>
        <div class="pano-menu-mobile"></div>

        <nav id="nav1" class="engloba-nav-bar transition">
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <ul class="nav-itens">
                <li>
                    <a class="nav-item nav-active" href="http://localhost:8080/TrabalhoWeb2/clienteJsps/clienteInicio.jsp">
                        Inicio
                    </a>
                </li>
                <li >
                    <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/clienteJsps/clienteNovoAtendimento.jsp">
                        Novo Atendimento
                    </a>
                </li>
                <li >
                    <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/clienteJsps/clientePerfil.jsp">
                        Perfil
                    </a>
                </li>
                <li >
                    <a class="nav-item" href="#">
                        Logout
                    </a>
                </li>
            </ul>
        </nav>


        <header id="nav2" class="top-header transition" >
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <div class="button-menu">
                <i class="open fas fa-bars"></i>
                <i class="close fas fa-times"></i>
            </div>
        </header>

        <section class="section-banner">
            <div class="container">
                <div class="banner">
                    <h1 class="title-usuario"> Atendimento </h1>
                </div>
            </div>
        </section>

        <!--Estatisticas-->

        <section class="section">
            <div class="container">
                <h2>Informações</h2>
                <div class="atendimento-info">
                    <div class="campo">
                        Data/hora:
                        <span>10/09/2018</span>
                    </div>
                    <div class="campo">
                        Cliente:
                        <span>Bárbara Vidal</span>
                    </div>
                    <div class="campo">
                        Produto
                        <span>Hidratação</span>
                    </div>
                    <div class="campo">
                        Tipo de atendimento:
                        <span>Reclamação</span>
                    </div>
                    <div class="campo">
                        Descrição
                        <span>Curabitur facilisis ultrices sapien eget sodales. Morbi molestie mi dui. Fusce sit amet odio eget libero luctus placerat. Maecenas a commodo mauris. Etiam consequat pulvinar nisi, et auctor velit ornare quis. Nullam tincidunt sodales feugiat. Etiam non porttitor mi, et elementum est. Suspendisse molestie nunc ut turpis sodales, lobortis finibus metus dictum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed ullamcorper dolor eu massa laoreet scelerisque. Vivamus dolor justo, malesuada nec congue non, tempus in lorem. Mauris blandit lectus finibus lectus pulvinar, sit amet euismod risus luctus.</span>
                    </div>
                    <div class="campo">
                        Solução:
                        <span>Curabitur facilisis ultrices sapien eget sodales. Morbi molestie mi dui. Fusce sit amet odio eget libero luctus placerat. Maecenas a commodo mauris. Etiam consequat pulvinar nisi, et auctor velit ornare quis. Nullam tincidunt sodales feugiat. Etiam non porttitor mi, et elementum est. Suspendisse molestie nunc ut turpis sodales, lobortis finibus metus dictum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed ullamcorper dolor eu massa laoreet scelerisque. Vivamus dolor justo, malesuada nec congue non, tempus in lorem. Mauris blandit lectus finibus lectus pulvinar, sit amet euismod risus luctus.</span>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="novo">
                    <a class="btn btn-danger bottt btn-xs"  href="#" data-toggle="modal" data-target="#delete-modal">Resolver</a>
                </div>
            </div>
        </section>


        <footer class="footer">
            <div class="container">
                <img alt="embuste" src="../img/logo.png">
                <p>Criado com ♥ por Bárbara Erick e Lucas dos Santos</p>
            </div>
        </footer>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    </body>

</html>

