// Quando scrolar aparece o menu
window.onscroll = function() {scrollFunction()};

//scoll
var flag = true;
function moveSite(href) {
    flag = false;
    var hash = href.split("#").pop(); //o split separa o index.html#inicio em um array com as duas partes, e o pop seleciona o último item do array.
    var altura = $("#section-"+hash).position().top -60 ;
    $("html, body").animate({scrollTop:altura}, 800, function(){
        flag = true; //depois de animar troca pra true
    });
}

$(document).ready(function(){

    window.onscroll = function() {scrollFunction()};
	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#nav2").css ({"background-color": "#161616"});
		} else {
            $("#nav2").css ({"background-color": "transparent"});
		}
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#nav1").css ({"background-color": "#161616"});
		} else {
		    $("#nav1").css ({"background-color": "transparent"});
		}

	}

	$(".nav-item").on("click", function(){
			$(".nav-item").removeClass('nav-active');
			$(this).addClass('nav-active');
			moveSite($(this).attr("href"));
			$(".open").css("display", "block");
			$(".close").css("display", "none");
			$(".menu-mobile").stop().slideUp();
			$(".pano-menu-mobile").stop().fadeOut();
			flag = 0;
	});
	/** Menu mobile **/
	var flag = 0;
	$(".button-menu").on("click", function() {
		if(flag == 0) {
			$(".menu-mobile").stop().slideDown();
			$(".open").css("display", "none");
			$(".close").css("display", "block");
			$(".pano-menu-mobile").stop().fadeIn();
            $("#nav2").css ({"background-color": "#161616"});
			flag = 1;
		}else {
			$(".menu-mobile").stop().slideUp();
			$(".open").css("display", "block");
			$(".close").css("display", "none");
			$(".pano-menu-mobile").stop().fadeOut();
            $("#nav2").css ({"background-color": "transparent"});
			flag = 0;
		}
	});
	$(".pano-menu-mobile").on("click", function() {
		$(".menu-mobile").stop().slideUp();
		$(".open").css("display", "block");
		$(".close").css("display", "none");
		$(".pano-menu-mobile").stop().fadeOut();
		flag = 0;
	});

});


$(window).on("load", function() { //qdo já passa o hash no link já desloca a página para a posição
    if (window.location.hash != "") { //se for com a hash vazia não chama a função para deslocamento
        moveSite(window.location.hash);
    };
});

$(window).on("load scroll resize", function() { //trocar hash e cor do menu normal, no mobile cabeçalho fica parado em cima
    var scrollY = window.pageYOffset;

    /** Ajustando hash com o scroll do mouse **/
    var top1 = $("#section-home").position().top - 60;
    var top2 = $("#section-pay-now").position().top - 60;
    var top3 = $("#section-parceiros").position().top - 60;
    var top4 = $("#section-negociar").position().top - 60;
    var top5 = $("#section-vantagens").position().top - 60;
    var top6 = $("#section-faq").position().top - 60;
	var top7 = $("#section-contato").position().top - 250;

    if(scrollY >= 0 && scrollY < top2) {
        var selecionado = $(".nav-itens").children().eq(0).children('.nav-item');

        var hash = selecionado.attr("href");
        window.location.hash = hash.split("#").pop();

        $(".nav-item").removeClass("nav-active");
        selecionado.addClass("nav-active");
    }
    else if(scrollY >= top2 && scrollY < top3) {
        var selecionado = $(".nav-itens").children().eq(1).children('.nav-item');

        var hash = selecionado.attr("href");
        window.location.hash = hash.split("#").pop();

        $(".nav-item").removeClass("nav-active");
        selecionado.addClass("nav-active");
    }
	else if(scrollY >= top3 && scrollY < top4) {
		var selecionado = $(".nav-itens").children().eq(2).children('.nav-item');

		var hash = selecionado.attr("href");
		window.location.hash = hash.split("#").pop();

		$(".nav-item").removeClass("nav-active");
		selecionado.addClass("nav-active");
	}
	else if(scrollY >= top4 && scrollY < top5) {
		var selecionado = $(".nav-itens").children().eq(3).children('.nav-item');

		var hash = selecionado.attr("href");
		window.location.hash = hash.split("#").pop();

		$(".nav-item").removeClass("nav-active");
		selecionado.addClass("nav-active");
	}
	else if(scrollY >= top5 && scrollY < top6) {
		var selecionado = $(".nav-itens").children().eq(4).children('.nav-item');

		var hash = selecionado.attr("href");
		window.location.hash = hash.split("#").pop();

		$(".nav-item").removeClass("nav-active");
		selecionado.addClass("nav-active");
	}
	else if(scrollY >= top6 && scrollY < top7) {
		var selecionado = $(".nav-itens").children().eq(5).children('.nav-item');

		var hash = selecionado.attr("href");
		window.location.hash = hash.split("#").pop();

		$(".nav-item").removeClass("nav-active");
		selecionado.addClass("nav-active");
	}
    else if(scrollY >= top7) {
        var selecionado = $(".nav-itens").children().eq(6).children('.nav-item');

        var hash = selecionado.attr("href");
        window.location.hash = hash.split("#").pop();

        $(".nav-item").removeClass("nav-active");
        selecionado.addClass("nav-active");
    }
});
